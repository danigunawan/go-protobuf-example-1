# https://medium.com/swlh/supercharge-your-rest-apis-with-protobuf-b38d3d7a28d3
compile-proto:
	protoc -I proto/echo/ --go_out=proto/echo/ echo.proto

# TEST MARSHAL & UNMARSHAL
run-test:
	make run-test --directory=./server

# REST ENDPOINT 
run-server:
	make run-server --directory=./server

# CLIENT
run-client:
	make run-client --directory=./client

######## COMPARE ######
# REST ENDPOINT 
run-server-compare:
	make run-server-compare --directory=./server

run-server-compare-with-package:
	make run-server-compare-with-package --directory=./server

run-client-compare:
	make run-client-compare --directory=./client
####### END COMPARE #######

# COMMIT, PUSH
git-push:
	git add . && git commit -m "update" && git push -u origin master