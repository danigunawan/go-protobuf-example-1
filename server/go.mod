module protobuf-demo

go 1.16

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	gitlab.com/danigunawan/go-protobuf-example-1 v0.0.0-20210721144357-02c09a16159e // indirect
	// gitlab.com/danigunawan/go-protobuf-example-1 v0.0.0-20210721144357-02c09a16159e // indirect
	google.golang.org/grpc/examples v0.0.0-20210720175814-65cabd74d8e1 // indirect
)
